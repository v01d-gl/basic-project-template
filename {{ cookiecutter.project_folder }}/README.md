# {{ cookiecutter.project_name }}

## How To Run

### Prepare your environment (once)

Install [uv](https://docs.astral.sh/uv/)

Using scoop

```bash
scoop install uv
```

Without scoop

```ps1
powershell -ExecutionPolicy ByPass -c "irm https://astral.sh/uv/install.ps1 | iex"
```

### Run

#### Using [Make](https://www.gnu.org/software/make/)

```bash
make
```

#### If Make is not installed

```bash
uv run python src/main.py
```
